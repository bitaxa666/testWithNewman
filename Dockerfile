FROM node:8.9.3
MAINTAINER mihaylukvv@gmail.com

RUN apt-get update
RUN apt-get install -y npm
# RUN mkdir projecttest
# RUN cd projecttest
RUN npm install -g http-server
RUN git clone https://github.com/cornflourblue/angular-registration-login-example.git
RUN cd angular-registration-login-example/
RUN ls
# RUN http-server
EXPOSE 8080

# CMD ["/bin/bash"]
CMD ["http-server"]
